<?php

namespace Skying\Admin\Middleware;

use Skying\Admin\Form;
use Skying\Admin\Grid;
use Illuminate\Http\Request;

class BootstrapMiddleware
{
    public function handle(Request $request, \Closure $next)
    {
        Form::registerBuiltinFields();

        Form::collectFieldAssets();

        Grid::registerColumnDisplayer();

        if (file_exists($bootstrap = admin_path('bootstrap.php'))) {
            require $bootstrap;
        }

        return $next($request);
    }
}
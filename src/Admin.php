<?php

namespace Skying\Admin;

use Closure;
use Skying\Admin\Auth\Database\Menu;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;
use InvalidArgumentException;
use Request;
use Storage;
use Cookie;
use Session;
use Response;
use DB;

/**
 * Class Admin.
 */
class Admin
{
    /**
     * @var Navbar
     */
    protected $navbar;

    /**
     * @var array
     */
    public static $script = [];

    /**
     * @var array
     */
    public static $css = [];

    /**
     * @var array
     */
    public static $js = [];

    /**
     * @param $model
     *
     * @return mixed
     */
    public function getModel($model)
    {
        if ($model instanceof EloquentModel) {
            return $model;
        }

        if (is_string($model) && class_exists($model)) {
            return $this->getModel(new $model());
        }

        throw new InvalidArgumentException("$model is not a valid model");
    }

    /**
     * Get namespace of controllers.
     *
     * @return string
     */
    public function controllerNamespace()
    {
        $directory = config('admin.directory');

        return ltrim(implode('\\',
              array_map('ucfirst',
                  explode(DIRECTORY_SEPARATOR, str_replace(app()->basePath(), '', $directory)))), '\\')
              .'\\Controllers';
    }


    /**
     * Admin url.
     *
     * @param $url
     *
     * @return string
     */
    public static function url($url)
    {
        $prefix = (string) config('admin.prefix');

        if (empty($prefix) || $prefix == '/') {
            return '/'.trim($url, '/');
        }

        return "/$prefix/".trim($url, '/');
    }

    /**
     * Left sider-bar menu.
     *
     * @return array
     */
    public function menu()
    {
        return (new Menu())->toTree();
    }

    /**
     * Get admin title.
     *
     * @return Config
     */
    public function title()
    {
        return config('admin.title');
    }

    /**
     * Get current login user.
     *
     * @return mixed
     */
    public function user()
    {
        return Auth::guard('admin')->user();
    }

 

    public function registerAuthRoutes()
    {
        $attributes = [
            'prefix'        => config('admin.prefix'),
            'namespace'     => 'Skying\Admin\Controllers',
            'middleware'    => ['web', 'admin'],
        ];

        Route::group($attributes, function ($router) {
                $attributes = ['middleware' => 'admin.permission:allow,administrator'];
            

                ###这里大概是只有站张可以访问
                /* @var \Illuminate\Routing\Router $router */
                $router->group($attributes, function ($router) {
                    $router->resources([
                        'auth/users'       => 'UserController',
                        'auth/roles'       => 'RoleController',
                        'auth/permissions' => 'PermissionController',
                        'auth/menu'        => 'MenuController',
                        'auth/logs'        => 'LogController',
                         
                    ]);
                
            });

            $router->get('auth/login', 'AuthController@getLogin');
            $router->post('auth/login', 'AuthController@postLogin');
            $router->get('auth/logout', 'AuthController@getLogout');
            $router->get('auth/setting', 'AuthController@getSetting');
            $router->put('auth/setting', 'AuthController@putSetting');
 
        });
    }

}

<?php

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Skying\Admin\Facades\Admin;
use Skying\Admin\Layout\Content;

 

if (!function_exists('admin_path')) {

    /**
     * Get admin path.
     *
     * @param string $path
     *
     * @return string
     */
    function admin_path($path = '')
    {
        return ucfirst(config('admin.directory')).($path ? DIRECTORY_SEPARATOR.$path : $path);
    }
}

if (!function_exists('admin_url')) {
    /**
     * Get admin url.
     *
     * @param string $url
     *
     * @return string
     */
    function admin_url($url = '')
    {
        $prefix = trim(config('admin.prefix'), '/');

        return url($prefix ? "/$prefix" : '').'/'.trim($url, '/');
    }
}

if (!function_exists('is_pjax')) {

    /**
     * Get ispjax.
     *
     *
     * @return bool
     */
    function is_pjax()
    {
        return array_key_exists('HTTP_X_PJAX', $_SERVER) && $_SERVER['HTTP_X_PJAX'];
    } 
} 



if (!function_exists('admin_config')) {

    /**
     * Get admin config.
     * @param string $key    
     * @return string
     */
    function admin_config($key)
    {
        return config('admin.'.$key);
    } 
} 


if (!function_exists('app_config')) {

    /**
     * Get app config.
     * @param string $key    
     * @return string
     */
    function app_config($key)
    {
        return admin_config('app'.$key);
    } 
} 


if (!function_exists('app_route_config')) {

    /**
     * Get app config.
     * @param string $key    
     * @return string
     */
    function app_route_config($key)
    {
        return app_config('route.'.$key);
    } 
} 

 


if (!function_exists('tomedia')) 
{

    function tomedia($src){
        if (empty($src)) return '';
        if (strpos($src, 'http://') || strpos($src, 'https://')) {
            return $src;
        }
        return asset($src);
    }

} 


if (!function_exists('set_medias')) 
{

    function set_medias($srcs,$fields = '' ){
       foreach ($srcs as $key => &$value) {
            if(is_array($fields)){
               foreach ($fields as $field) {
                     $value[$field] = tomedia($value[$field]);
               }
            }else if(!empty($fields)){
                $value[$fields] = tomedia($value[$fields]);
            }else{
                 $value = tomedia($value);
            }
       }

       return $srcs;
    }

} 


if (!function_exists('show_json')) 
{

    function show_json($code = 0,$response = array() ,$msg = '')
    {
        
        $msg = !empty($msg) ? $msg : config('code.'.$code);
        $json = compact('code','response','msg');
        $log = new Logger(' api response json ');
        $log->pushHandler(
            new StreamHandler(
                storage_path('logs/api/'.date('Y-m-d')), 
                Logger::INFO
            )
        );
        $log->addInfo(json_encode($json) );
        return response()->json($json)->header('Content-Type', 'application/json');
    }

} 


if (!function_exists('inject_check')) 
{    
   function inject_check($value)
   {
       return preg_match('/select|inert|update|delete|\'|\/\*|\*|\.\.\/|\.\/|UNION|into|load_file|outfile|updatexml|extractvalue/i', $value );
   }
}



if(!function_exists('isPhoneNum'))
{
    function isPhoneNum($mobile)
    {
        return  preg_match("/^1[1345678]{1}\d{9}$/",$mobile);
    }

}


if(!function_exists('error'))
{
    function error($msg)
    {
        return ['status'=>'failure','msg'=>$msg];
    }
}

 

if(!function_exists('success'))
{
    function success($msg)
    {
        return ['status'=>'success','msg'=>$msg];
    }
}

if(!function_exists('is_error'))
{
    function is_error($error)
    {
        return   isset($error['status']) &&  $error['status'] == 'failure'  ;
    }
}
 
//校验密码：只能输入6-12个字母、数字、下划线 
if(!function_exists('is_passwd'))
{
    function is_passwd($passwd)
    {
        return  preg_match("/^(\w){6,12}$/",$passwd);
    }
}


 
//校验密码：只能输入6-12个字母、数字、下划线 
if(!function_exists('message'))
{
    function message($string  = '' , $url = '' , $type = 'success' , $model = false ,$time = '3')
    {   
        $data = compact('string','url','type','time');
        if($model)  return view('admin::shop.public.message',$data);
        return Admin::content(function(Content $content) use ($data){ 
           $content->body( view('admin::shop.public.message',$data ) );  
        });
         
    }
}













 





 


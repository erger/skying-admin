<?php

namespace Skying\Admin\Facades;

use Illuminate\Support\Facades\Facade;

class Admin extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \Skying\Admin\Admin::class;
    }
}

<?php

namespace Skying\Admin\Providers;

use Skying\Admin\Facades\Admin;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use Skying\Admin\Services\ResourcesService;
use Validator;
use Goods;
use DB;
use Skying\Admin\Models\GoodsOption; 

class AdminServiceProvider extends ServiceProvider
{
 

    /**
     * @var array
     */
    protected $commands = [
        'Skying\Admin\Commands\MakeCommand',
        'Skying\Admin\Commands\MenuCommand',
        'Skying\Admin\Commands\InstallCommand',
        'Skying\Admin\Commands\UninstallCommand',
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'admin.auth'        => \Skying\Admin\Middleware\Authenticate::class,
        'admin.pjax'        => \Skying\Admin\Middleware\PjaxMiddleware::class,
        'admin.log'         => \Skying\Admin\Middleware\OperationLog::class,
        'admin.permission'  => \Skying\Admin\Middleware\PermissionMiddleware::class,
        'admin.bootstrap'   => \Skying\Admin\Middleware\BootstrapMiddleware::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'admin' => [
            'admin.auth',
            'admin.pjax',
            'admin.log',
            'admin.bootstrap',
        ],
    ];

    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../../lang/', 'admin');

        $this->publishes([__DIR__.'/../../config/admin.php' => config_path('admin.php')], 'skying-admin');
        
      //  $this->publishes([__DIR__.'/../../assets' => public_path('packages/admin')], 'skying-admin');

        Admin::registerAuthRoutes();
 
        if (file_exists($routes = admin_path('routes.php'))) {
            require $routes;
        }
    }

 

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->booting(function () {
            $loader = AliasLoader::getInstance();
            $loader->alias('Admin', \Skying\Admin\Facades\Admin::class);
            $loader->alias('ResourcesService',\Skying\Admin\Facades\ResourcesFacade::class);
 
            if (is_null(config('auth.guards.admin'))) {
                $this->setupAuth();
            }
            $this->setUrlConfig();
        });

        $this->registerRouteMiddleware();
        $this->commands($this->commands);
 
         
    }



    public function setUrlConfig()
    {
        config([
            'auth.resources.list' => 'admin/resources/list',
            'auth.resources.upload' => 'admin/resources/upload',
            'auth.resources.delete' => 'admin/resources/delete',
        ]);

    }

    /**
     * Setup auth configuration.
     *
     * @return void
     */
    protected function setupAuth()
    {
        config([
            'auth.guards.admin.driver'    => 'session',
            'auth.guards.admin.provider'  => 'admin',
            'auth.providers.admin.driver' => 'eloquent',
            'auth.providers.admin.model'  => 'Skying\Admin\Auth\Database\Administrator',
        ]);
    }

    /**
     * Register the route middleware.
     *
     * @return void
     */
    protected function registerRouteMiddleware()
    {
        // register route middleware.
        foreach ($this->routeMiddleware as $key => $middleware) {
            app('router')->aliasMiddleware($key, $middleware);
        }

        // register middleware group.
        foreach ($this->middlewareGroups as $key => $middleware) {
            app('router')->middlewareGroup($key, $middleware);
        }
    }
}
